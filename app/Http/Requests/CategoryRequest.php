<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => (request()->method == 'POST')?'required|unique:categories':'required|unique:categories,title,' . $this->id,
            'slug' => (request()->method == 'POST')?'required|unique:categories':'required|unique:categories,slug,' . $this->id,
            'rank' => 'required|integer',
            'image_file' => (request()->method == 'POST')?'required|mimes:jpg,bmp,png':'',
        ];
    }
}
