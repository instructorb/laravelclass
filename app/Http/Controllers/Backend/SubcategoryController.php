<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends BackendBaseController
{
    public $base_view = 'backend.subcategory.';
    public $base_route = 'backend.subcategory.';
    public $module = 'SubCategory';
    public $folder = 'Subcategory';

    public function __construct(){
        $this->model = new Subcategory();
    }
    function create(){
        $data['categories'] = Category::select('title','id')->get();
        return view($this->__loadDataToView($this->base_view . 'create'),compact('data'));
    }

    function index()
    {
        $data['records'] = $this->model->all();
        return view($this->__loadDataToView($this->base_view .'index'),compact('data'));
    }

    function store(Request $request)
    {
        $validated = $request->validate([
            'category_id' => 'required',
            'title' => 'required|max:255',
            'slug' => 'required',
            'rank' => 'required|integer',
            'image_file' => 'required|mimes:jpg,bmp,png'
        ]);
        if ($request->hasFile('image_file')){
            $file = $request->file('image_file');
            $iname = uniqid() . '_' . $file->getClientOriginalName();
            $file->move('images/subcategory/',$iname);
            $request->request->add(['image' => $iname]);
        }
        $request->request->add(['created_by' => auth()->user()->id]);
        $this->model->create($request->all());
        return redirect()->route($this->base_route . 'index');
    }

    function  show($id){
       $data['record'] =  $this->model->find($id);
       if (!$data['record']){
           return redirect()->route($this->base_route .'index');
       }
       return view($this->__loadDataToView($this->base_view . 'show'),compact('data'));
    }

    function destroy($id){
       $data =  $this->model->find($id);
       $data->delete();
        return redirect()->route($this->base_route .'index');
    }

    function edit($id){
        $data['record'] =  $this->model->find($id);
        return view($this->__loadDataToView($this->base_view .'edit'),compact('data'));
    }

    function update(Request $request,$id)
    {
        $data = $this->model->find($id);
        $validated = $request->validate([
            'title' => 'required|max:255',
            'slug' => 'required',
            'rank' => 'required|integer'
        ]);
        if ($request->hasFile('image_file')){
            $file = $request->file('image_file');
            $iname = uniqid() . '_' . $file->getClientOriginalName();
            $file->move('images/category/',$iname);
            $request->request->add(['image' => $iname]);
        }
        $request->request->add(['updated_by' => auth()->user()->id]);
        $data->update($request->all());
        return redirect()->route($this->base_route .'index');
    }

    function trash()
    {
        $data = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_view .'trash'),compact('data'));
    }

    function restore($id)
    {
        $data = $this->model->onlyTrashed()->find($id)->restore();
        return redirect()->route($this->base_route .'trash');
    }

    function forceDelete($id)
    {
        $data = $this->model->onlyTrashed()->find($id)->forceDelete();
        return redirect()->route($this->base_route .'trash');
    }
}
