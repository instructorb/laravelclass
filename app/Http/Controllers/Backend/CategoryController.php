<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends BackendBaseController
{
    public $base_view = 'backend.category.';
    public $base_route = 'backend.category.';
    public $module = 'Category';
    public $folder = 'category';

    public function __construct(){
        $this->model = new Category();
    }
    function create(){
        return view($this->__loadDataToView($this->base_view . 'create'));
    }

    function index()
    {
        $data['records'] = $this->model->paginate(2);
        return view($this->__loadDataToView($this->base_view .'index'),compact('data'));
    }

    function store(CategoryRequest $request)
    {
        if ($request->hasFile('image_file')){
            $file = $request->file('image_file');
            $iname = uniqid() . '_' . $file->getClientOriginalName();
            $file->move('images/category/',$iname);
            $request->request->add(['image' => $iname]);
        }
        $request->request->add(['created_by' => auth()->user()->id]);
        $this->model->create($request->all());
        return redirect()->route($this->base_route . 'index');
    }

    function  show($id){
       $data['record'] =  $this->model->find($id);
       if (!$data['record']){
           return redirect()->route($this->base_route .'index');
       }
       return view($this->__loadDataToView($this->base_view . 'show'),compact('data'));
    }

    function destroy($id){
       $data =  $this->model->find($id);
       $data->delete();
        return redirect()->route($this->base_route .'index');
    }

    function edit($id){
        $data['record'] =  $this->model->find($id);
        return view($this->__loadDataToView($this->base_view .'edit'),compact('data'));
    }

    function update(CategoryRequest $request,$id)
    {
        $data = $this->model->find($id);
        if ($request->hasFile('image_file')){
            $file = $request->file('image_file');
            $iname = uniqid() . '_' . $file->getClientOriginalName();
            $file->move('images/category/',$iname);
            $request->request->add(['image' => $iname]);
        }
        $request->request->add(['updated_by' => auth()->user()->id]);
        $data->update($request->all());
        return redirect()->route($this->base_route .'index');
    }

    function trash()
    {
        $data = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_view .'trash'),compact('data'));
    }

    function restore($id)
    {
        $data = $this->model->onlyTrashed()->find($id)->restore();
        return redirect()->route($this->base_route .'trash');
    }

    function forceDelete($id)
    {
        $data = $this->model->onlyTrashed()->find($id)->forceDelete();
        return redirect()->route($this->base_route .'trash');
    }
}
