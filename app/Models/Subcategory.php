<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'subcategories';

    protected $fillable = ['category_id','title','rank','image','description','slug','meta_keyword','meta_description','created_by','updated_by','status'];
}
