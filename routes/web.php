<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware('auth')->name('backend.')->prefix('backend')->group(function(){
    Route::get('category/trash', [\App\Http\Controllers\Backend\CategoryController::class, 'trash'])->name('category.trash');
    Route::get('category/restore/{id}', [\App\Http\Controllers\Backend\CategoryController::class, 'restore'])->name('category.restore');
    Route::delete('category/force_delete/{id}', [\App\Http\Controllers\Backend\CategoryController::class, 'forceDelete'])->name('category.force_delete');


    Route::get('category/create', [\App\Http\Controllers\Backend\CategoryController::class, 'create'])->name('category.create');
    Route::get('category', [\App\Http\Controllers\Backend\CategoryController::class, 'index'])->name('category.index');
    Route::post('category', [\App\Http\Controllers\Backend\CategoryController::class, 'store'])->name('category.store');
    Route::get('category/{id}', [\App\Http\Controllers\Backend\CategoryController::class, 'show'])->name('category.show');
    Route::delete('category/{id}', [\App\Http\Controllers\Backend\CategoryController::class, 'destroy'])->name('category.destroy');
    Route::get('category/{id}/edit', [\App\Http\Controllers\Backend\CategoryController::class, 'edit'])->name('category.edit');
    Route::put('category/{id}', [\App\Http\Controllers\Backend\CategoryController::class, 'update'])->name('category.update');


    Route::get('subcategory/trash', [\App\Http\Controllers\Backend\SubcategoryController::class, 'trash'])->name('subcategory.trash');
    Route::get('subcategory/restore/{id}', [\App\Http\Controllers\Backend\SubcategoryController::class, 'restore'])->name('subcategory.restore');
    Route::delete('subcategory/force_delete/{id}', [\App\Http\Controllers\Backend\SubcategoryController::class, 'forceDelete'])->name('subcategory.force_delete');

    Route::get('subcategory/create', [\App\Http\Controllers\Backend\SubcategoryController::class, 'create'])->name('subcategory.create');
    Route::get('subcategory', [\App\Http\Controllers\Backend\SubcategoryController::class, 'index'])->name('subcategory.index');
    Route::post('subcategory', [\App\Http\Controllers\Backend\SubcategoryController::class, 'store'])->name('subcategory.store');
    Route::get('subcategory/{id}', [\App\Http\Controllers\Backend\SubcategoryController::class, 'show'])->name('subcategory.show');
    Route::delete('subcategory/{id}', [\App\Http\Controllers\Backend\SubcategoryController::class, 'destroy'])->name('subcategory.destroy');
    Route::get('subcategory/{id}/edit', [\App\Http\Controllers\Backend\SubcategoryController::class, 'edit'])->name('subcategory.edit');
    Route::put('subcategory/{id}', [\App\Http\Controllers\Backend\SubcategoryController::class, 'update'])->name('subcategory.update');
});

