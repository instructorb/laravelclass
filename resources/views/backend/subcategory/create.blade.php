@extends('layouts.backend')
@section('title',$module . ' Create')
@section('breadcrumb')
    <div class="col-sm-6">
        <h1>{{$module}} Management</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item ">{{$module}}</li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </div>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create {{$module}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="card-body">
                <form action="{{route($base_route . 'store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select class="form-control" name="category_id" id="category_id">
                            <option value="">Select Category</option>
                            @foreach($data['categories'] as $cat)
                                <option value="{{$cat->id}}">{{$cat->title}}</option>
                            @endforeach
                        </select>
                        @include('backend.includes.error_display',['field' => 'category_id'])
                    </div>
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" class="form-control" name="title">
                        @include('backend.includes.error_display',['field' => 'title'])
                    </div>
                    <div class="form-group">
                        <label for="">Slug</label>
                        <input type="text" class="form-control" name="slug">
                        @include('backend.includes.error_display',['field' => 'slug'])
                    </div>
                    <div class="form-group">
                        <label for="">Rank</label>
                        <input type="text" class="form-control" name="rank">
                        @include('backend.includes.error_display',['field' => 'rank'])
                    </div>
                    <div class="form-group">
                        <label for="">Image</label>
                        <input type="file" class="form-control" name="image_file" accept="image/*" onchange="loadFile(event)">
                        @include('backend.includes.error_display',['field' => 'image_file'])

                        <img id="output" height="100px"/>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="description" id="" class="form-control" cols="30" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Meta Description</label>
                        <textarea name="meta_description" id="" class="form-control" cols="30" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Meta Keyword</label>
                        <textarea name="meta_keyword" id="" class="form-control" cols="30" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="1" name="status" id="flexRadioDefault1">
                            <label class="form-check-label" for="flexRadioDefault1">
                               Active
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" value="0" id="flexRadioDefault2" checked>
                            <label class="form-check-label" for="flexRadioDefault2">
                                De Active
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Save {{$module}}">
                        <input type="reset" class="btn btn-danger" value="Clear">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
@endsection
