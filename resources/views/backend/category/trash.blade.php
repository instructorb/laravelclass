@extends('layouts.backend')
@section('title','Category List')
@section('breadcrumb')
    <div class="col-sm-6">
        <h1>Category Management</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item ">Category</li>
            <li class="breadcrumb-item active">Trash</li>
        </ol>
    </div>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List Category Trash
                    <a href="{{route('backend.category.index')}}" class="btn btn-success">List</a>
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Rank</th>
                        <th>Action</th>
                    </tr>
                    @foreach($data as $category)
                    <tr>
                        <td>{{$loop->index +1}}</td>
                        <td>{{$category->title}}</td>
                        <td><img src="{{asset('images/category/' . $category->image)}}" alt="" width="100"></td>
                        <td>{{$category->rank}}</td>
                        <td>
                            <a href="{{route('backend.category.restore',$category->id)}}" class="btn btn-info">Restore</a>
                            <form method="post" action="{{route('backend.category.force_delete',$category->id)}}" >
                                <input  type="hidden" name="_method" value="delete">
                                @csrf
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>


                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
