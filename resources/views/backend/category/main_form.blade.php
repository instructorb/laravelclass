<div class="form-group">
    {!! Form::label('title', 'Title'); !!}
    {!! Form::text('title', null,['class' => 'form-control']); !!}
    @include('backend.includes.error_display',['field' => 'title'])
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug', null,['class' => 'form-control']); !!}
    @include('backend.includes.error_display',['field' => 'slug'])
</div>
<div class="form-group">
    {!! Form::label('rank', 'Rank'); !!}
    {!! Form::number('rank', null,['class' => 'form-control']); !!}
    @include('backend.includes.error_display',['field' => 'rank'])
</div>
<div class="form-group">
    {!! Form::label('image', 'Image'); !!}
    {!! Form::file('image_file',['class' => 'form-control', 'accept' => 'image/*','onchange' => "loadFile(event)"] ); !!}
    @include('backend.includes.error_display',['field' => 'image_file'])
    @if(isset($data['record']))
        <img  id="output" src="{{asset('images/category/' . $data['record']->image)}}" alt="" height="100">
    @else
        <img id="output" height="100px"/>
    @endif


</div>
<div class="form-group">
    {!! Form::label('description', 'Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control' ,'rows' => '3']); !!}
</div>
<div class="form-group">
    {!! Form::label('meta_description', 'Meta Description'); !!}
    {!! Form::textarea('meta_description', null,['class' => 'form-control' ,'rows' => '3']); !!}
</div>
<div class="form-group">
    {!! Form::label('meta_keyword', 'Meta Keyword'); !!}
    {!! Form::textarea('meta_keyword', null,['class' => 'form-control' ,'rows' => '3']); !!}
</div>
<div class="form-group">
    {!! Form::label('status', 'Status'); !!}

    <div class="form-check">
        {!! Form::radio('status', 1,false,['class' => 'form-check-input']); !!}
        Active
    </div>
    <div class="form-check">
        {!! Form::radio('status', 0,true,['class' => 'form-check-input']); !!}
        De Active
    </div>
</div>
