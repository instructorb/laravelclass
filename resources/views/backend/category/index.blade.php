@extends('layouts.backend')
@section('title',$module . ' List')
@section('breadcrumb')
    <div class="col-sm-6">
        <h1>{{$module}} Management</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item ">{{$module}}</li>
            <li class="breadcrumb-item active">List</li>
        </ol>
    </div>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List {{$module}}</h3>
                <div class="card-tools">
                    <a href="{{route($base_route . 'trash')}}" class="btn btn-danger">Trash</a>
                    <a href="{{route($base_route . 'create')}}" class="btn btn-info">Create</a>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Rank</th>
                        <th>Action</th>
                    </tr>
                    @foreach($data['records'] as $record)
                    <tr>
                        <td>{{$loop->index +1}}</td>
                        <td>{{$record->title}}</td>
                        <td><img src="{{asset('images/' .$folder.'/' . $record->image)}}" alt="" width="100"></td>
                        <td>{{$record->rank}}</td>
                        <td>
                            <a href="{{route($base_route . 'show',$record->id)}}" class="btn btn-info">View</a>
                            <a href="{{route($base_route . 'edit',$record->id)}}" class="btn btn-warning">Edit</a>
                            <form method="post" action="{{route($base_route . 'destroy',$record->id)}}" style="display: inline-block;" >
                                <input  type="hidden" name="_method" value="delete">
                                @csrf
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="5">
                            <div class="d-flex justify-content-right">
                                {!! $data['records']->links() !!}
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection
