@extends('layouts.backend')
@section('title','Category Create')
@section('breadcrumb')
    <div class="col-sm-6">
        <h1>Category Management</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item ">Category</li>
            <li class="breadcrumb-item active">Edit</li>
        </ol>
    </div>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Edit Category</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="card-body">
               {!! Form::model($data['record'],['route' => ['backend.category.update', $data['record']->id], 'method' => 'put','files' => true]) !!}
                @include('backend.category.main_form')
                {!! Form::hidden('id',$data['record']->id) !!}
                <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Save Category">
                        <input type="reset" class="btn btn-danger" value="Clear">
                    </div>
                </form>
            </div>
            <!-- /.card-body -->

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
    @endsection
